medcom.model.net
================

This is the Medcom model made easy - probably the only project of its kind and purpose: To make it easier for vendors in the danish healthcare sector to use the Medcom standards and thereby interact with each other.

This project fetches the current versions of the Medcom standards using svn (**so please have a svn command line client installed before running the script**), generates classes from the xsd's and parses them into a .NET library - thereby making it way easier to use this part of the protocol stack. Just call the script.bat from a **Visual Studio** ***Developer Command Prompt*** and you should have a library in just about no time.

Happy codin'.